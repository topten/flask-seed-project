from app.container import web


@web.route('/', methods=['GET'])
def index():
    return '<h1>It works!</h1>'
