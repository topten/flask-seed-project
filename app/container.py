import config
from flask import Flask

web = Flask(__name__)
web.config.from_object(config)

from app.models.entities import db

db.init_app(web)
db.app = web

# Define your services here

# Register url rules here
from app.endpoints.pages import *
