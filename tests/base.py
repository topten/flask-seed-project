import unittest
import os
import config


class TestCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        from app import container
        self.container = container

        self._check_environment()
        self._set_up_database()

    def _check_environment(self):
        """
        Проверяет что тест будет выполнен в тестовом окружении, иначе пропускает тест
        Защита от дурака, чтобы не запустить случайно тесты на рабочей базе
        """
        if os.getenv('FORCE_ANY_ENVIRONMENTS_IN_TESTS'):
            return

        if config.ENV != 'test':
            self.skipTest('You can run tests only in "test" environment. '
                          'If you want run tests in other environments, create env variable '
                          'SSB_FORCE_ANY_ENVIRONMENTS_IN_TESTS=1')

    def _set_up_database(self):
        db = self.container.db
        db.session.commit()
        db.drop_all()
        db.create_all()