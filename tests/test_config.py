"""
Пример класса для автоматического тестирования
"""

from tests.base import TestCase


class TestConfigCase(TestCase):
    def test_that_is_test_env(self):
        from config import IS_TEST_ENV
        self.assertEqual(IS_TEST_ENV, 1, 'config.IS_TEST_ENV should equals to 1')
