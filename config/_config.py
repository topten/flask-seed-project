import os, logging

########################################################################################################################
# Основные настройки
########################################################################################################################
from config.common import *

########################################################################################################################
# Окружение для запуска приложения
########################################################################################################################
__ALLOWED_ENVIRONMENTS = {'test', 'dev', 'stage', 'prod'}

ENV = os.getenv('APP_ENV')

if ENV not in __ALLOWED_ENVIRONMENTS:
    raise AssertionError(
        'Invalid environment for the app. '
        'Make sure that you specify ENV_VAR APP_ENV. '
        'Allowed values for APP_ENV are {allowed_values}'.format(allowed_values=", ".join(__ALLOWED_ENVIRONMENTS))
    )

logging.warning('Running app in "{}" environment'.format(ENV))

########################################################################################################################
# Переопределение настроек запуска приложения в зависимости от окружения
########################################################################################################################
if ENV == 'test':
    # noinspection PyUnresolvedReferences
    from config.test import *
elif ENV == 'dev':
    # noinspection PyUnresolvedReferences
    from config.dev import *
elif ENV == 'stage':
    # noinspection PyUnresolvedReferences
    from config.stage import *
elif ENV == 'prod':
    # noinspection PyUnresolvedReferences
    from config.prod import *

logging.warning('Environment variable values:')
import sys
current_module = sys.modules[__name__]
for var_name, var_value in sorted(list(current_module.__dict__.items())):
    if not var_name.startswith('__') and var_name.isupper():
        logging.warning('{}: {}'.format(var_name, var_value))
