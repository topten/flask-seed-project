# docker build --build-arg SSH_KEY="$(< ~/.ssh/key)" .
FROM python:3.5

MAINTAINER Artem Kolesnikov <kolesnikov.dejust@gmail.com>

ARG SSH_KEY

ADD ./requirements.txt /app/requirements.txt
WORKDIR /app

RUN mkdir /root/.ssh \
    && touch /root/.ssh/known_hosts \
    && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts \
    && eval `ssh-agent -s` \
    && echo "$SSH_KEY" | ssh-add - \
    && pip install -r requirements.txt
