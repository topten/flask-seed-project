#!/bin/bash

if [[ "$1" =~ ^(prod|stage|dev)$ ]]; then
    docker-compose -f $(pwd)/docker-compose.yml -f $(pwd)/docker-compose.$1.yml "${@:2}"
else
    echo "WRONG ENV NAME: $1"
fi