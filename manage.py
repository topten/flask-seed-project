from app.container import web, db
from app.commands import hello_world


from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

if __name__ == "__main__":
    manager = Manager(web)
    migrate = Migrate(web, db)

    manager.add_command('db', MigrateCommand)
    manager.add_command('hello_world', hello_world.HelloWorldCommand)

    manager.run()